//
//  MessageLog.h
//  E219 Prototype
//
//  Created by Marty on 06/04/2015.
//  Copyright (c) 2015 Martin Grant. All rights reserved.
//

#pragma once

#include <vector>
#include <string>
#include <ctime>
#include <fstream>
#include <iostream>

#ifdef _WIN32
#include <Windows.h>
#else
#include <sys/stat.h>
#endif

class MessageLog
{
public:
    MessageLog(bool writeToFile);
    ~MessageLog();

public:
    void logString(std::string message);
    void writeLogToFile();
    void writeLogToConsole();
    
private:
    std::string getDate();
    std::string getTime();
    
private:
    std::vector<std::string> m_messageLog;
    bool m_writeToFile;
    
};