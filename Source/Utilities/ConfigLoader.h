//
//  ConfigLoader.h
//  E219 Engine
//
//  Created by Marty on 23/05/2015.
//  Copyright (c) 2015 Martin Grant. All rights reserved.
//

#pragma once

#include <string>
#include <fstream>
#include <unordered_map>

class ConfigLoader
{
    /* Class Constructors & Destructors */
public:
    ConfigLoader(std::string configFile);
    ~ConfigLoader();
    
public:
    std::string getValue(std::string key);
    
private:
    std::string m_configFile;
    std::unordered_map<std::string, std::string> m_configMap;
};