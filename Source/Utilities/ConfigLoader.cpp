//
//  ConfigLoader.cpp
//  E219 Engine
//
//  Created by Marty on 23/05/2015.
//  Copyright (c) 2015 Martin Grant. All rights reserved.
//

#include "ConfigLoader.h"

ConfigLoader::ConfigLoader(std::string configFile) : m_configFile(configFile)
{
    std::string line;
    std::ifstream file (m_configFile);
    
    if (file.is_open())
    {
        while (getline(file,line))
        {
            std::string key, value;
            
            key = line.substr(0, line.find(":"));
            value = line.substr(line.find(":") + 1, line.size());
            
            m_configMap[key] = value;
        }
        
        file.close();
    }
}


ConfigLoader::~ConfigLoader()
{
    
}


std::string ConfigLoader::getValue(std::string key)
{
    for (auto iterator = m_configMap.begin(); iterator != m_configMap.end(); ++iterator)
    {
        if (iterator->first == key)
        {
            return iterator->second;
        }
    }
    
    return nullptr;
}