//
//  MessageLog.cpp
//  E219 Prototype
//
//  Created by Marty on 06/04/2015.
//  Copyright (c) 2015 Martin Grant. All rights reserved.
//

#include "MessageLog.h"

MessageLog::MessageLog(bool writeToFile) : m_writeToFile(writeToFile)
{
}


MessageLog::~MessageLog()
{
    if (m_writeToFile == true)
    {
        writeLogToFile();
    }
    
    m_messageLog.clear();
}


void MessageLog::logString(std::string message)
{
    m_messageLog.push_back(std::string(getTime() + " - " + message));
    
    std::cout << m_messageLog.back() << std::endl;
}


void MessageLog::writeLogToFile()
{
    std::cout << "Writing MessageLog to file." << std::endl;
    
    std::string filePath = "../Logs/";
	
    // todo - os check
	//CreateDirectory((LPCWSTR)filePath.c_str(), NULL);
	//mkdir("../Logs/", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
    // todo - add time too, otherwise log will get overwritten if ran twice in same day
    std::string fileName = "MessageLog-" + getDate() + ".txt";
    
    std::ofstream file;
    file.open(filePath + fileName);
    
    for (unsigned int index = 0; index < m_messageLog.size(); ++index)
    {
        file << m_messageLog[index] << "\n";
    }
    
    file.close();
}


void MessageLog::writeLogToConsole()
{
    std::cout << "Console Print for MessageLog:" << std::endl;
    
    for (unsigned int index = 0; index < m_messageLog.size(); ++index)
    {
        std::cout << m_messageLog[index] << std::endl;
    }
}


std::string MessageLog::getDate()
{
    time_t t = time(0);
    struct tm* now = localtime(&t);
    
    std::string date = std::to_string(now->tm_mday) + "." + std::to_string(now->tm_mon + 1) + "." + std::to_string(now->tm_year + 1900);
    
    return date;
}


std::string MessageLog::getTime()
{
    time_t t = time(0);
    struct tm* now = localtime(&t);
    
    std::string time = std::to_string(now->tm_hour) + ":" + std::to_string(now ->tm_min) + ":" + std::to_string(now->tm_sec);
    
    return time;
}