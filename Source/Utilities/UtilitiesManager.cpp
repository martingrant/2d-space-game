#include "UtilitiesManager.h"


UtilitiesManager::UtilitiesManager()
{
}


UtilitiesManager::~UtilitiesManager()
{
}


void UtilitiesManager::registerService(std::shared_ptr<WindowManager> windowManager)
{
	m_windowManager = windowManager;
}


std::shared_ptr<WindowManager> UtilitiesManager::getWindowManager()
{
	return m_windowManager;
}


void UtilitiesManager::registerService(std::shared_ptr<Input> inputManager)
{
	m_inputManager = inputManager;
}


std::shared_ptr<Input> UtilitiesManager::getInputManager()
{
	return m_inputManager;
}



void UtilitiesManager::registerService(std::shared_ptr<TimeManager> timeManager)
{
	m_timeManager = timeManager;
}


std::shared_ptr<TimeManager> UtilitiesManager::getTimeManager()
{
	return m_timeManager;
}


void UtilitiesManager::registerService(std::shared_ptr<MessageLog> messageLog)
{
    m_messageLog = messageLog;
}


std::shared_ptr<MessageLog> UtilitiesManager::getMessageLog()
{
    return m_messageLog;
}


void UtilitiesManager::registerService(std::shared_ptr<ConfigLoader> configLoader)
{
    m_configLoader = configLoader;
}


std::shared_ptr<ConfigLoader> UtilitiesManager::getConfigLoader()
{
    return m_configLoader;
}