#pragma once

#include <memory>
#include <utility>

#include "../Utilities/UtilitiesManager.h"
#include "../Screens/WindowManager.h"
#include "../Screens/SDLWindowManager.h"
#include "../Input/SDLInput.h"
#include "../Time/TimeManager.h"
#include "../Entities/EntityManager.h"
#include "../Utilities/MessageLog.h"
#include "../Utilities/ConfigLoader.h"

class Application
{
public:
	/* Class Constructors & Destructors*/
	Application();
	~Application();
    
public:
	/* General Public Methods */
	bool run();

private:
	bool m_running;

	std::shared_ptr<WindowManager> m_windowManager;
	std::shared_ptr<SDLInput> m_inputService;
	std::shared_ptr<EntityManager> m_entityManager;
};

