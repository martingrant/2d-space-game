#include "Application.h"

Application::Application() : m_running(true)
{
    UtilitiesManager::getInstance().registerService(std::shared_ptr<MessageLog>(new MessageLog(true)));
 
    UtilitiesManager::getInstance().getMessageLog()->logString("System constructing: Application");
    
    UtilitiesManager::getInstance().registerService(std::shared_ptr<ConfigLoader>(new ConfigLoader("Config/config.txt")));
    
    m_inputService = std::shared_ptr<SDLInput>(new SDLInput(4));
    UtilitiesManager::getInstance().registerService(m_inputService);

	m_windowManager = std::shared_ptr<WindowManager>(new SDLWindowManager());
	UtilitiesManager::getInstance().registerService(m_windowManager);
    
	UtilitiesManager::getInstance().registerService(std::shared_ptr<TimeManager>(new TimeManager(30, 5)));
    
    UtilitiesManager::getInstance().getMessageLog()->logString("System constucted: Application");
}


Application::~Application()
{
    UtilitiesManager::getInstance().getMessageLog()->logString("System destructed: Application.");
}


bool Application::run()
{
    m_inputService->update();
    m_running = m_windowManager->update();
    m_windowManager->render();
    
	return m_running;
}