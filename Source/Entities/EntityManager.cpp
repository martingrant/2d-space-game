//
//  EntityManager.cpp
//  E219 Prototype
//
//  Created by Marty on 07/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "EntityManager.h"


EntityManager::EntityManager()
{
	std::cout << "System constructing: EntityManager." << std::endl << std::endl;

    m_entityVector.reserve(std::stoi(UtilitiesManager::getInstance().getConfigLoader()->getValue("maximumEntities")));

	for (unsigned int index = 0; index < std::stoi(UtilitiesManager::getInstance().getConfigLoader()->getValue("maximumEntities")); ++index)
    {
		m_entityVector.emplace_back(index);
    }
    
	m_ComponentFactory = std::unique_ptr<ComponentFactory>(new ComponentFactory());

	std::cout << "System constructed: EntityManager." << std::endl << std::endl;
}


EntityManager::~EntityManager()
{
	std::cout << "System destructed: EntityManager." << std::endl << std::endl;
}


unsigned int EntityManager::getNumberOfEntities()
{
    return (unsigned int)m_entityVector.size();
}


void EntityManager::update()
{
    for (unsigned int index = 0; index < m_entityVector.size(); ++index)
    {
        m_entityVector[index].update();
    }
}


void EntityManager::createEntity(std::string entityName)
{
    if (entityVectorFull() == false)
    {
        if (checkUniqueName(entityName))
        {
            for (std::vector<Entity>::iterator iterator = m_entityVector.begin(); iterator != m_entityVector.end(); ++iterator)
            {
                if (iterator->isAssigned() == false)
                {
                    iterator->assign();
                    iterator->setEntityName(entityName);

                    break;
                }
            }
        }
        else
        {
            std::cout << std::endl << "Could not create Entity: " << entityName << ", another Entity exists with this name already." << std::endl;
        }
    }
    else
    {
        std::cout << std::endl << "Could not create Entity: " << entityName << ", Entity Vector is full." << std::endl;
    }
}


void EntityManager::deleteEntity(std::string entityName)
{
    for (std::vector<Entity>::iterator iterator = m_entityVector.begin(); iterator != m_entityVector.end(); ++iterator)
    {
        if (iterator->getEntityName() == entityName)
        {
            if (iterator->isAssigned() == true)
            {
                iterator->unassign();
                break;
            }
        }
    }
}


Entity* EntityManager::getEntity(std::string entityName)
{
	for (unsigned int index = 0; index < m_entityVector.size(); ++index)
	{
		if (m_entityVector[index].getEntityName() == entityName)
		{
			return &m_entityVector[index];
		}
	}
    
    return nullptr;
}


Entity* EntityManager::getEntity(unsigned int entityID)
{
    for (unsigned int index = 0; index < m_entityVector.size(); ++index)
    {
        if (m_entityVector[index].getEntityID() == entityID)
        {
            return &m_entityVector[index];
        }
    }
    
    return nullptr;
}


void EntityManager::addComponentToEntityID(unsigned int entityID, ComponentTypes componentType)
{
	for (unsigned int index = 0; index < m_entityVector.size(); ++index)
	{
		if (m_entityVector[index].getEntityID() == entityID)
		{
			addComponentToEntity(index, componentType);
			break;
		}
	}
}


void EntityManager::addComponentToEntityName(std::string entityName, ComponentTypes componentType)
{
	for (unsigned int index = 0; index < m_entityVector.size(); ++index)
	{
		if (m_entityVector[index].getEntityName() == entityName)
		{
			addComponentToEntity(index, componentType);
			break;
		}
	}
}


std::vector<Entity>& EntityManager::getEntityVector()
{
    return m_entityVector;
}


void EntityManager::addComponentToEntity(unsigned int entity, ComponentTypes componentType)
{
	m_entityVector[entity].addComponent(m_ComponentFactory->orderNewComponent(componentType));
}


bool EntityManager::entityVectorFull()
{
	for (unsigned int index = 0; index < m_entityVector.size(); ++index)
    {
        if (m_entityVector[index].isAssigned() == false)
        {
			return false;
        }
    }

	return true;
}


bool EntityManager::checkUniqueName(std::string entityName)
{
	for (unsigned int index = 0; index < m_entityVector.size(); ++index)
    {
        if (m_entityVector[index].getEntityName() == entityName)
        {
            return false;
        }
    }
    
    return true;
}