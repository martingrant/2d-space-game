 //
//  Entity.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "Entity.h"


Entity::Entity(unsigned int entityID) : m_entityID(entityID), m_assigned(false), m_entityName("")
{
	std::cout << "Entity Constructed (ID: " << m_entityID << ")." << std::endl << std::endl;
    m_shouldDestroy = false;
    
    m_destroy = false;
}


Entity::~Entity()
{
    
}


void Entity::update()
{
	for (unsigned int index = 0; index < m_componentVector.size(); ++index)
	{
		m_componentVector[index]->update();
        sendMessageToComponents(m_componentVector[index]->sendMessage());
        
        if (m_componentVector[index]->sendMessage() == "dead")
            unassign();
	}
}


void Entity::sendMessageToComponents(std::string message)
{
    for (unsigned int index = 0; index < m_componentVector.size(); ++index)
    {
        m_componentVector[index]->receiveMessage(message);
    }
}


void Entity::setEntityName(std::string entityName)
{
    m_entityName = entityName;
	std::cout << std::endl << "Entity (ID: " << m_entityID << ") name set: " << m_entityName << std::endl;
}


std::string Entity::getEntityName()
{
    return m_entityName;
}


unsigned int Entity::getEntityID()
{
    return m_entityID;
}


bool Entity::isAssigned()
{
    return m_assigned;
}


void Entity::assign()
{
    if (m_assigned == false)
    {
        m_assigned = true;
        
        if (m_entityName.empty())
        {
			std::cout << std::endl << "Entity (ID: " << m_entityID << ") assigned." << std::endl;
        }
        else
        {
			std::cout << std::endl << "Entity (ID: " << m_entityID << ", Name: " << m_entityName << ") assigned." << std::endl;
        }
    }
    else
    {
        if (m_entityName.empty())
        {
			std::cout << std::endl << "Entity (ID: " << m_entityID << ") cannot be assigned. It is already assigned." << std::endl;
        }
        else
        {
			std::cout << std::endl << "Entity (ID: " << m_entityID << ", Name: " << m_entityName << ") cannot be assigned. It is already assigned." << std::endl;
        }
    }
}


void Entity::unassign()
{
    m_assigned = false;
    m_destroy = false;
    
    for (unsigned int i = 0; i < m_componentVector.size(); ++i)
    {
        delete m_componentVector[i];
    }
    m_componentVector.clear();
    
    if (m_entityName.empty())
    {
		std::cout << std::endl << "Entity (ID: " << m_entityID << ") unassigned." << std::endl;
    }
    else
    {
		std::cout << std::endl << "Entity (ID: " << m_entityID << ", Name: " << m_entityName << ") unassigned." << std::endl;
    }
    
    m_entityName.clear();
}


void Entity::addComponent(Component* newComponent)
{
    newComponent->setOwner(this);
	m_componentVector.push_back(newComponent);
}


Component* Entity::getComponent(ComponentTypes componentType)
{
    for (unsigned int index = 0; index < m_componentVector.size(); ++index)
	{
		if (m_componentVector[index]->getComponentType() == componentType)
		{
			return m_componentVector[index];
		}
	}
    
    return nullptr;
}