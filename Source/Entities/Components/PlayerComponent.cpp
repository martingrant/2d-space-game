//
//  PhysicalComponent.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "PlayerComponent.h"


PlayerComponent::PlayerComponent()
{
	m_componentType = PLAYER;
}


PlayerComponent::~PlayerComponent()
{
    
}


void PlayerComponent::update()
{
    if (m_input->getKeyState("w")) m_messageToSend = "KW";
    else
    if (m_input->getKeyState("s")) m_messageToSend = "KS";
    else
    if (m_input->getKeyState("a")) m_messageToSend = "KA";
    else
    if (m_input->getKeyState("d")) m_messageToSend = "KD";
    else 
        m_messageToSend = "";
}


void PlayerComponent::getUserInput(std::shared_ptr<Input> input)
{
    m_input = input;
}


void PlayerComponent::receiveMessage(std::string message)
{
    m_messageToSend = message;
}


std::string PlayerComponent::sendMessage()
{
    return m_messageToSend;
}