#include "PhysicsComponent.h"


PhysicsComponent::PhysicsComponent()
{
	m_componentType = PHYSICS;
    m_hit = false;
}


PhysicsComponent::~PhysicsComponent()
{
    
}


void PhysicsComponent::update()
{
    if (m_hit)
    {
        m_messageToSend = "hit";
        m_hit = false;
    } else m_messageToSend = "";
}


void PhysicsComponent::receiveMessage(std::string message)
{
    //b2Vec2 point = m_body->GetLocalCenter();
    b2Vec2 point = b2Vec2(0.0f, 0.0f);
    m_body->SetFixedRotation(true);
    //b2Vec2 point = b2Vec2(0.0f, 0.0f);
    
    if (message == "KW")
    {
        //m_body->ApplyLinearImpulse(b2Vec2(0.0f, -20000.0f), b2Vec2(0.0f, 0.0f), true);
        m_body->ApplyLinearImpulse(b2Vec2(0.0f, -2.0f), point, true);
    }
    
    if (message == "KS")
    {
        //m_body->ApplyLinearImpulse(b2Vec2(0.0f, 20000.0f), b2Vec2(0.0f, 0.0f), true);
        m_body->ApplyLinearImpulse(b2Vec2(0.0f, 2.0f), point, true);
    }
    
    if (message == "KA")
    {
        //m_body ->ApplyLinearImpulse(b2Vec2(-20000.0f, 0.0f), b2Vec2(0.0f, 0.0f), true);
        m_body->ApplyLinearImpulse(b2Vec2(-2.0f, 0.0f), point, true);
    }
    
    if (message == "KD")
    {
        //m_body->ApplyLinearImpulse(b2Vec2(20000.0f, 0.0f), b2Vec2(0.0f, 0.0f), true);
        m_body->ApplyLinearImpulse(b2Vec2(2.0f, 0.0f), point, true);
    }
}


std::string PhysicsComponent::sendMessage()
{
    return m_messageToSend;
}


void PhysicsComponent::setProperties(b2World& world, std::string bodyType, float positionX, float positionY, float width, float height, float density, float friction, float restitution, bool isBullet)
{
    m_positionX = positionX;
    m_positionY = positionY;
    m_width = width;
    m_height = height;
    m_density = density;
    m_friction = friction;
    m_restitution = restitution;
    
    b2BodyDef bodyDef;
    
    if (bodyType == "dynamic")
    {
        bodyDef.type = b2_dynamicBody;
    }
    else if (bodyType == "static")
    {
        bodyDef.type = b2_staticBody;
    }

    bodyDef.position.Set(m_positionX, m_positionY);
    
    m_body = world.CreateBody(&bodyDef);

    b2PolygonShape dynamicBox;
    //dynamicBox.SetAsBox(m_width / 2, m_height / 2);
    dynamicBox.SetAsBox(1, 1);
    
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &dynamicBox;
    fixtureDef.density = m_density;
    fixtureDef.friction = m_friction;
    fixtureDef.restitution = m_restitution;
    
    m_body->SetLinearDamping(2.0f);
    
    m_body->CreateFixture(&fixtureDef);
    
    m_body->SetUserData(this);
    m_isBullet = isBullet;
}


b2Body* PhysicsComponent::getBody()
{
    return m_body;
}


float PhysicsComponent::getPositionX()
{
    return m_body->GetPosition().x;
}


float PhysicsComponent::getPositionY()
{
    return m_body->GetPosition().y;
}


float PhysicsComponent::getAngle()
{
    return m_body->GetAngle();
}


float PhysicsComponent::getWidth()
{
    return m_width;
}


float PhysicsComponent::getHeight()
{
    return m_height;
}


float PhysicsComponent::getDensity()
{
    return m_density;
}


float PhysicsComponent::getFrictin()
{
    return m_friction;
}


float PhysicsComponent::getRestitution()
{
    return m_restitution;
}


void PhysicsComponent::applyLinearImpluse(b2Vec2 impulse, b2Vec2 point)
{
    m_body->ApplyLinearImpulse(impulse, point, true);
}