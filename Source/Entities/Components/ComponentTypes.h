#pragma once 

enum ComponentTypes {
	PHYSICAL,
	TEXTURE,
    PLAYER,
    PHYSICS,
    HEALTH,
};