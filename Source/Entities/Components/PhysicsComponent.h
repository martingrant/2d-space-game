#pragma once

#include <iostream>

#include <Box2D/Box2D.h>

#include "Component.h"
#include "ComponentTypes.h"

class PhysicsComponent : public Component
{
    /* Class Constructors & Destructors */
public:
	PhysicsComponent();
	virtual ~PhysicsComponent();
    
	/* General Public Methods*/
public:
    virtual void update();
    
    virtual void receiveMessage(std::string message);
    virtual std::string sendMessage();
    
public:
    void setProperties(b2World& world, std::string bodyType, float positionX, float positionY, float width, float height, float density, float friction, float restitution, bool isBullet);
    b2Body* getBody();
    
    float getPositionX();
    float getPositionY();
    float getAngle();
    float getWidth();
    float getHeight();
    float getDensity();
    float getFrictin();
    float getRestitution();
    
    void applyLinearImpluse(b2Vec2 impulse, b2Vec2 point);
    
    void hasBeenHit() { m_hit = true; }
    
    bool isBullet() { return m_isBullet; }
    
private:
    b2Body* m_body;
    float m_positionX;
    float m_positionY;
    float m_width;
    float m_height;
    float m_density;
    float m_friction;
    float m_restitution;
    
    bool m_isBullet;
    
    bool m_hit;

};