//
//  ComponentFactory.cpp
//  E219 Prototype
//
//  Created by Marty on 09/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "ComponentFactory.h"

ComponentFactory::ComponentFactory()
{
	std::cout << "System constructed: ComponentFactory." << std::endl << std::endl;
}


ComponentFactory::~ComponentFactory()
{
	std::cout << "System destructing: ComponentFactory." << std::endl << std::endl;
}


Component* ComponentFactory::orderNewComponent(ComponentTypes componentType)
{
	Component* newComponent = nullptr;
    
	switch (componentType)
	{
		case PHYSICAL:
			newComponent = new PhysicalComponent();
			break;
		case TEXTURE:
			newComponent = new TextureComponent();
            break;
        case PLAYER:
            newComponent = new PlayerComponent();
            break;
        case PHYSICS:
            newComponent = new PhysicsComponent();
            break;
        case HEALTH:
            newComponent = new HealthComponent();
            break;
	}

	return newComponent;
}