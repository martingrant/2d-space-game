//
//  HealthComponent.hpp
//  SpaceGame2D
//
//  Created by Marty on 21/11/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#pragma once

#include <iostream>

#include "Component.h"

class HealthComponent : public Component
{
    /* Class Constructors & Destructors */
public:
    HealthComponent();
    virtual ~HealthComponent();
    
    /* General Public Methods*/
public:
    virtual void update();
    
    virtual void receiveMessage(std::string message);
    virtual std::string sendMessage();
    
public:
    void setHealth(unsigned int health);
    void incrementHealth(unsigned int health);
    unsigned int getHealth();
    
private:
    unsigned int m_health;
};