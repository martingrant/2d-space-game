//
//  ComponentFactory.h
//  E219 Prototype
//
//  Created by Marty on 09/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <vector>
#include <memory>
#include <iostream>

#include "ComponentTypes.h"
#include "Component.h"
#include "PhysicalComponent.h"
#include "TextureComponent.h"
#include "PlayerComponent.h"
#include "PhysicsComponent.h"
#include "HealthComponent.h"

class ComponentFactory
{
    /* Class Constructors & Destructors */
public:
	ComponentFactory();
    ~ComponentFactory();
    
    /* Component Management */
public:
	Component* orderNewComponent(ComponentTypes componentType);
};