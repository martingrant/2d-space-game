//
//  HealthComponent.cpp
//  SpaceGame2D
//
//  Created by Marty on 21/11/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#include "HealthComponent.h"


HealthComponent::HealthComponent()
{
    m_componentType = HEALTH;
}


HealthComponent::~HealthComponent()
{
    
}


void HealthComponent::update()
{
    if (m_health == 0)
        m_messageToSend = "dead";
}


void HealthComponent::receiveMessage(std::string message)
{
    if (message == "hit")
    {
        incrementHealth(-10);
        std::cout << "hp = " << m_health << std::endl;
    }
    
}


std::string HealthComponent::sendMessage()
{
    return m_messageToSend;
}


void HealthComponent::setHealth(unsigned int health)
{
    m_health = health;
}


void HealthComponent::incrementHealth(unsigned int health)
{
    m_health += health;
}


unsigned int HealthComponent::getHealth()
{
    return m_health;
}