//
//  PhysicalComponent.h
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <string>
#include <iostream>

#ifdef _WIN32
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#elif __APPLE__
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#endif

#include "Component.h"
#include "ComponentTypes.h"


class PhysicalComponent : public Component
{
    /* Class Constructors & Destructors */
public:
	PhysicalComponent();
    virtual ~PhysicalComponent();
    
	/* General Public Methods*/
public:
	virtual void update();
    
    virtual void receiveMessage(std::string message);
    virtual std::string sendMessage();

public:
    /* Physical Property Methods */
    void setPosition(glm::vec2 newPosition);
	void setPositionX(float newPositionX);
	void setPositionY(float newPositionY);

	glm::vec2 getPosition();

    void setScale(glm::vec2 newScale);
	void setScaleX(float newScaleX);
	void setScaleY(float newScaleY);

	glm::vec2 getScale();
    
	void setRotationX(float newRotationX);
	void setRotationY(float newRotationY);

	glm::vec2 getRotation();
    
    void setHeading(float newHeading);
    float getHeading();
    
    glm::vec2 getVelocity() { return m_velocity; }
    void setVelocity(glm::vec2 newVelocity) { m_velocity = newVelocity; }

private:
	glm::vec2 m_position;
	glm::vec2 m_scale;
	glm::vec2 m_rotation;
    
    float m_acceleration;
    //float m_velocity;
    glm::vec2 m_velocity;
    
    float m_heading;
};