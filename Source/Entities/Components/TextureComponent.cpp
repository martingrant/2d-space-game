//
//  PhysicalComponent.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "TextureComponent.h"


TextureComponent::TextureComponent()
{
	m_componentType = TEXTURE;
}


TextureComponent::~TextureComponent()
{
    
}


void TextureComponent::update()
{

}


void TextureComponent::receiveMessage(std::string message)
{
    
}


std::string TextureComponent::sendMessage()
{
    return m_messageToSend;
}


void TextureComponent::setTextureName(std::string textureName)
{
	m_textureName = textureName;
}


std::string TextureComponent::getTextureName()
{
	return m_textureName;
}