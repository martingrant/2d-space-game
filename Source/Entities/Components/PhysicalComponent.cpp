//
//  PhysicalComponent.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "PhysicalComponent.h"


PhysicalComponent::PhysicalComponent() : m_heading(0.0f), m_velocity(10.0f), m_acceleration(0.1f)
{
    m_componentType = PHYSICAL;
}


PhysicalComponent::~PhysicalComponent()
{
    
}


void PhysicalComponent::update()
{

}


void PhysicalComponent::receiveMessage(std::string message)
{
    /*
    if (message[0] == 'K')
    {
		if (message[1] == 'W')
		{
            m_position.y -= m_velocity.y;
		}

		if (message[1] == 'S')
		{
            m_position.y += m_velocity.y;
		}

		if (message[1] == 'A') 
		{
            m_position.x -= m_velocity.x;
		}

		if (message[1] == 'D')
		{
            m_position.x += m_velocity.x;
		}
    }*/
}


std::string PhysicalComponent::sendMessage()
{
    return m_messageToSend;
}


void PhysicalComponent::setPosition(glm::vec2 newPosition)
{
    m_position = newPosition;
}


void PhysicalComponent::setPositionX(float newPositionX)
{
	m_position.x = newPositionX;
}


void PhysicalComponent::setPositionY(float newPositionY)
{
	m_position.y = newPositionY;
}


glm::vec2 PhysicalComponent::getPosition()
{
	return m_position;
}


void PhysicalComponent::setScale(glm::vec2 newScale)
{
    m_scale = newScale;
}


void PhysicalComponent::setScaleX(float newScaleX)
{
	m_scale.x = newScaleX;
}


void PhysicalComponent::setScaleY(float newScaleY)
{
	m_scale.y = newScaleY;
}


glm::vec2 PhysicalComponent::getScale()
{
	return m_scale;
}


void PhysicalComponent::setRotationX(float newRotationX)
{
	m_rotation.x = newRotationX;
}


void PhysicalComponent::setRotationY(float newRotationY)
{
	m_rotation.y = newRotationY;
}


glm::vec2 PhysicalComponent::getRotation()
{
	return m_rotation;
}


void PhysicalComponent::setHeading(float newHeading)
{
    m_heading = newHeading;
}


float PhysicalComponent::getHeading()
{
    return m_heading;
}