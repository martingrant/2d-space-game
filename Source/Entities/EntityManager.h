//
//  EntityManager.h
//  E219 Prototype
//
//  Created by Marty on 07/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <vector>
#include <memory>
#include <string>

#include "Entity.h"
#include "Components/ComponentFactory.h"
#include "Components/ComponentTypes.h"
#include "../Utilities/UtilitiesManager.h"

class EntityManager
{
    /* Class Constructor & Destructor */
public:
    EntityManager();
    ~EntityManager();
    
    /* Entity Management */
public:
    unsigned int getNumberOfEntities();
    
    void update();
    
    void createEntity(std::string entityName);
    void deleteEntity(std::string entityName);
	Entity* getEntity(std::string entityName);
    Entity* getEntity(unsigned int entityID);
	void addComponentToEntityID(unsigned int entityID, ComponentTypes componentType);
    void addComponentToEntityName(std::string entityName, ComponentTypes componentType);
    
    std::vector<Entity>& getEntityVector();
    
private:
	void addComponentToEntity(unsigned int entity, ComponentTypes componentType);

    std::vector<Entity> m_entityVector;
    
    std::unique_ptr<ComponentFactory> m_ComponentFactory;
    
private:
    bool entityVectorFull();
    bool checkUniqueName(std::string entityName);
};