//
//  Entity.h
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <iostream>
#include <unordered_map>
#include <memory>
#include <vector>
#include <string>

#include "Components/ComponentTypes.h"
#include "Components/Component.h"

class Entity
{
    /* Class Constructors & Destructors */
public:
	Entity() {}
    Entity(unsigned int entityID);
    ~Entity();

	/* General Public Methods*/
public:
	void update();
    void sendMessageToComponents(std::string message);

    /* Entity Management */
public:
    void setEntityName(std::string entityName);
    std::string getEntityName();
    unsigned int getEntityID();
    bool isAssigned();
    void assign();
    void unassign();
    
    bool m_shouldDestroy;
    
    void setDestroy(bool val) { m_destroy = val; }
    bool getDestroy() { return m_destroy; }
    
private:
    std::string m_entityName;
    unsigned int m_entityID;
    bool m_assigned;
    
    /* Component Management */
public:
	void addComponent(Component* newComponent);
	Component* getComponent(ComponentTypes componentType);
    
private:
	std::vector<Component*> m_componentVector;
    
    bool m_destroy;
};

