#include "SDLController.h"


SDLController::SDLController(unsigned int controllerID) : m_controllerID(controllerID)
{
	std::cout << "System constructing: SDLController." << std::endl << std::endl;

	m_controllerStatus = false;

	openController();

	std::cout << "System constructed: SDLController." << std::endl << std::endl;
}


SDLController::~SDLController(void)
{
	std::cout << "System destructing: SDLController." << std::endl << std::endl;

	closeController();

	m_controller = nullptr;
	m_haptic = nullptr;

	std::cout << "System destructed: SDLController. " << std::endl << std::endl;
}


void SDLController::setControllerID(int controllerID)
{
	m_controllerID = controllerID;
}


bool SDLController::openController()
{
	bool status = false;

	// Test to open controller, if true open it's haptic if supported
	if ((m_controller = SDL_GameControllerOpen(m_controllerID)))
	{
		// Check if index is a supported device
		if (SDL_IsGameController(m_controllerID))
		{
			// Print opened controller information
			std::cout << "Controller opened on index: " << m_controllerID << ". Device name: " << SDL_GameControllerName(m_controller) << std::endl << std::endl;;
			status = true;
			m_controllerStatus = true;

			//openHaptic();
		}
		else
		{
			std::cout << "Device unsupported on index: " << m_controllerID << " failed to open." << std::endl << std::endl;
			status = false;
		}
	}
	else
	{
		// Print information if controller failed to open
		std::cout << "Controller on index: " << m_controllerID << " failed to open." << std::endl << std::endl;
		status = false;
	}
	
	return status;
}


bool SDLController::closeController()
{
	bool status = false;

	// Check if controller is connected and opened
	if (m_controllerStatus == true)
	{
		if (SDL_GameControllerGetAttached(m_controller))
		{
			// Close haptic and then controller
			//closeHaptic();
			SDL_GameControllerClose(m_controller);

			status = true;
			m_controllerStatus = false;

			// Print controller information
			std::cout << "Controller closed on index: " << m_controllerID << ". Device name: " << SDL_GameControllerName(m_controller) << std::endl << std::endl;
		}
		else
		{
			// If controller not found, print information about controller index
			std::cout << "Close controller failed. Device not found on index: " << m_controllerID << ". Device name: " << SDL_GameControllerName(m_controller) << std::endl << std::endl;

			status = false;
		}
	}

	return status;
}


bool SDLController::getControllerStatus()
{
	if (SDL_GameControllerGetAttached(m_controller) == false)
	{
		m_controllerStatus = false;
	}
	else
	{
		m_controllerStatus = true;
	}

	return m_controllerStatus;
}


bool SDLController::openHaptic()
{
	bool status = false;

	// Create temp joystick pointer
	SDL_Joystick* tempJoystick = nullptr;

	// Exit if unable to access joystick device
	if (!SDL_JoystickOpen(m_controllerID))
	{
		std::cout << "Unable to access device on index: " << m_controllerID << ". Device name: " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
		return false;
	}
	else tempJoystick = SDL_JoystickOpen(m_controllerID);

	// Check if index is a supported device
	if (SDL_JoystickIsHaptic(tempJoystick))
	{
		// Point temp pointer to device index and open/init haptic
		if ((m_haptic = SDL_HapticOpen(m_controllerID)))
		{
			// Print opened haptic information
			std::cout << "Haptic opened on device on index: " << m_controllerID << std::endl << "Haptic: "
				<< SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
			status = true;

			// Init rumble on haptic
			if (SDL_HapticRumbleSupported(m_haptic))
			{
				SDL_HapticRumbleInit(m_haptic);
				std::cout << "Rumble initialized for haptic on device index: " << m_controllerID << std::endl << "Haptic: " <<
					SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
			}
			// Rumble not supported
			else
			{
				std::cout << "Rumble not supported for haptic on device index: " << m_controllerID << std::endl << "Haptic: " <<
					SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
			}
		}
		// Couldn't open haptic
		else
		{
			std::cout << "Haptic for index on index: " << m_controllerID << " failed to open. Device name:  " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
			status = false;
		}
	}
	// Device ain't got no haptic feedback
	else
	{
		std::cout << "Haptic not supported for device on index: " << m_controllerID << " failed to open. Device name:  " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
		status = false;
	}

	// Close joystick device (only needed for this method)
	SDL_JoystickClose(tempJoystick);
	tempJoystick = nullptr;

	return status;
}


bool SDLController::closeHaptic()
{
	bool status = false;

	// Check if index is a supported device
	if (SDL_HapticOpened(m_controllerID) == 1)
	{
		SDL_HapticStopAll(m_haptic);
		SDL_HapticClose(m_haptic);

		// Print haptic information
		std::cout << "Haptic closed for device on index: " << m_controllerID << std::endl << "Haptic: "
			<< SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
		status = true;
	}
	else
	{
		std::cout << "Haptic for device on index: " << m_controllerID << " failed to close. Device name:  " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
		status = false;
	}

	return status;
}


int SDLController::getControllerAxis(const char* axis)
{
	int axisValue = 0;

	// Check if controller is connected and opened
	if (m_controllerStatus == true)
	{
		if (SDL_GameControllerGetAxis(m_controller, SDL_GameControllerGetAxisFromString(axis)))
		{
			axisValue = SDL_GameControllerGetAxis(m_controller, SDL_GameControllerGetAxisFromString(axis));

			//std::cout << "Axis pushed on controller: " << controllerID << " (" << SDL_GameControllerNameForIndex(controllerID) << "): '"
			//<< SDL_GameControllerGetStringForAxis((SDL_GameControllerAxis) axis) << " ( " 
			//<< SDL_GameControllerGetAxis(tempController, (SDL_GameControllerAxis) axis) << " )'" << std::endl;
		}
	} //else std::cout << "Controller on index: " << m_controllerID << " not found or connected" << std::endl;

	return axisValue;
}


bool SDLController::getControllerButtonState(const char* button)
{
	bool status = false;

	// Check if controller is connected and opened
	if (m_controllerStatus == true)
	{
		if (SDL_GameControllerGetButton(m_controller, SDL_GameControllerGetButtonFromString(button)))
		{
			status = true;

			std::cout << "Button pressed on controller: " << m_controllerID << " (" << SDL_GameControllerNameForIndex(m_controllerID);
			//<< "): " << "'" << SDL_GameControllerGetStringForButton((SDL_GameControllerButton) button) << "'"<< std::endl;
		}
	} //else std::cout << "Controller on index: " << m_controllerID << " not found or connected" << std::endl;

	return status;
}
