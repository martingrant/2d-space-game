#pragma once

#include <utility>

class Input
{
public:
	/* Virtual Destructor */
	virtual ~Input(void) {}

public:
	/* General Public Methods */
	virtual void update() = 0;

public:
	/* Keyboard Methods */
	virtual bool getKeyState(const char* key) = 0;

public:
	/* Mouse Methods */
	virtual std::pair<int, int> getMousePosition() = 0;
	virtual std::pair<int, int> getMouseRelativePosition() = 0;
	virtual bool mouseButtonState(unsigned int buttonID) = 0;
	virtual int getMouseWheel() = 0;

public:
	/* Controller Methods*/
	virtual int getControllerAxis(unsigned int controllerID, const char* axis) = 0;
	virtual bool getControllerButtonState(unsigned int controllerID, const char* button) = 0;
	virtual bool onControllerButtonUp(unsigned int controllerID, const char* button) = 0;
	virtual bool onControllerButtonDown(unsigned int controllerID, const char* button) = 0;
};

