#include "SDLInput.h"


SDLInput::SDLInput(const unsigned int maximumNumberOfControllers) : m_maximumNumberOfControllers(maximumNumberOfControllers)
{
	std::cout << "System constructing: SDLInput." << std::endl << std::endl;

	if (SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC) != 0)
	{
		std::cout << "SDL_InitSubSystem: " << SDL_GetError() << std::endl;
	}

	// Call detect controllers to find out how many controlers are currently connected	
	m_connectedControllers = detectControllers();

	// Detect number of haptic devices;
	//detectHaptics();

	m_controllerVector.reserve(m_maximumNumberOfControllers);

	// Add a new controller to the controller map and open it for how many controllers have been detected
	for (int index = 0; index < m_maximumNumberOfControllers; ++index)
	{
		m_controllerVector.push_back(std::unique_ptr<SDLController>(new SDLController(index)));
	}

	std::cout << "System constructed: SDLInput." << std::endl << std::endl;
}


SDLInput::~SDLInput(void)
{
	std::cout << "System destructed: SDLInput." << std::endl << std::endl;
}


void SDLInput::update()
{
	SDL_PollEvent(&m_SDLEvent);
}


bool SDLInput::getKeyState(const char* key)
{
	bool status = false;

	//char* scanCode = strcat("SDL_Scancode", key);

	const Uint8* keyboard = SDL_GetKeyboardState(NULL);

	if (keyboard[SDL_GetScancodeFromName(key)]) {
		status = true;
		//std::cout << "Key pressed: " << SDL_GetScancodeName(scanCode) << " (Scancode: " << scanCode << ")" << std::endl; 
	}

	return status;
}


std::pair<int, int> SDLInput::getMousePosition()
{
	int x, y;
	SDL_GetMouseState(&x, &y);

	return std::pair<int, int>(x, y);
}


std::pair<int, int> SDLInput::getMouseRelativePosition()
{
	int x, y;
	SDL_GetRelativeMouseState(&x, &y);

	return std::pair<int, int>(x, y);
}


bool SDLInput::mouseButtonState(unsigned int buttonID)
{
	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(buttonID))
	{
		return true;
	}
	else
	{
		return false;
	}
}


int SDLInput::getMouseWheel()
{
	int wheelValue = 0;

	while (SDL_PollEvent(&m_SDLEvent)) {
		if (m_SDLEvent.type == SDL_MOUSEWHEEL) {
			std::cout << "Mouse wheel scrolled: " << m_SDLEvent.wheel.y << std::endl;
			wheelValue = m_SDLEvent.wheel.x;
		}
	}

	return wheelValue;
}


int SDLInput::getControllerAxis(unsigned int controllerID, const char* axis)
{
	int axisValue = 0;

	if (controllerID < m_connectedControllers)
    {
		axisValue = m_controllerVector[controllerID]->getControllerAxis(axis);
    }

	return axisValue;
}


bool SDLInput::getControllerButtonState(unsigned int controllerID, const char* button)
{
	bool buttonState = false;

	if (controllerID < m_connectedControllers)
    {
		buttonState = m_controllerVector[controllerID]->getControllerButtonState(button);
    }

	return buttonState;
}


bool SDLInput::onControllerButtonUp(unsigned int controllerID, const char* button)
{
	bool buttonState = false;

	if (controllerID < m_connectedControllers)
	{
		if (m_SDLEvent.cbutton.type == SDL_CONTROLLERBUTTONUP)
		{
			if (m_SDLEvent.cbutton.button == SDL_GameControllerGetAxisFromString(button))
			{
				buttonState = true;
			}
		}
	}

	return buttonState;
}


bool SDLInput::onControllerButtonDown(unsigned int controllerID, const char* button)
{
	bool buttonState = true;

	if (controllerID < m_connectedControllers)
	{
		if (m_controllerVector[controllerID]->getControllerButtonState(button) == false)
		{
			buttonState = false;
		}
		else
		{
			buttonState = true;
		}
	}

	return buttonState;
}


int SDLInput::detectControllers()
{
	int connectedControllers = SDL_NumJoysticks();

	//std::cout << std::endl << m_connectedControllers << " controller(s) detected." << std::endl;

	// Print number of controllers and their type if any are detected
	//if (m_connectedControllers > 0)
	//	for (int index = 0; index < m_connectedControllers; index++)
	//	std::cout << "Controller detected on index: " << index << ". Device name: " << SDL_GameControllerNameForIndex(index) << std::endl;

	return connectedControllers;
}


void SDLInput::detectHaptics()
{
	int numHaptics = SDL_NumHaptics();

	std::cout << numHaptics << " Haptic device(s) detected." << std::endl;

	// Print number of haptics and their type if any are detected
	if (numHaptics > 0)
	{
		for (int i = 0; i < numHaptics; ++i)
		{
			std::cout << "Haptic detected on device on index: " << i << ". Device name: " << SDL_HapticName(i) << std::endl;
		}
	}
}


bool SDLInput::openController(unsigned int controllerID)
{
	return m_controllerVector[controllerID]->openController();
}


bool SDLInput::closeController(unsigned int controllerID)
{
	return m_controllerVector[controllerID]->closeController();
}


void SDLInput::pollControllers()
{
	//// Test if controllers are removed, try to close them
	//if (m_SDLEvent.cdevice.type == SDL_CONTROLLERDEVICEREMOVED)
	//{
	//	for (std::vector<std::unique_ptr<SDLController>>::iterator iterator = m_controllerVector.begin(); iterator != m_controllerVector.end(); ++iterator)
	//	{
	//		if (/*!iterator->second &&*/ !iterator->second->getControllerStatus())
	//		{
	//			std::cout << "Controller " << iterator->second->getControllerID() << " disconneted." << std::endl;
	//			iterator->second->closeController();
	//		}
	//	}
	//}

	//// Check if controllers are connected, try to open them
	//if (m_SDLEvent.cdevice.type == SDL_CONTROLLERDEVICEADDED)
	//{
	//	for (std::unordered_map<int, std::unique_ptr<SDLController>>::iterator iterator = m_controllerList.begin(); iterator != m_controllerList.end(); ++iterator)
	//	{
	//		if (/*!iterator->second && */!iterator->second->getControllerStatus())
	//		{
	//			std::cout << "Controller " << iterator->second->getControllerID() << " connected." << std::endl;
	//			iterator->second->openController();
	//		}
	//	}
	//}
}