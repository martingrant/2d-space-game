#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

class Light
{
public:
	Light();
	Light(glm::vec4 ambient, glm::vec4 diffuse, glm::vec4 specular, glm::vec4 position, glm::vec3 coneDirection, GLfloat coneFallOff, glm::vec3 attenuation, int active, int type);
	~Light(void);

public:
	glm::vec4 getAmbient();
	glm::vec4 getDiffuse();
	glm::vec4 getSpecular();

	glm::vec4 getPosition();
	void setPositionX(GLfloat newPosition);
	void setPositionY(GLfloat newPosition);
	void setPositionZ(GLfloat newPosition);
	void setPositionW(GLfloat newPosition);

	glm::vec3 getConeDirection();
	void setConeDirectionX(GLfloat newDirection);
	void setConeDirectionY(GLfloat newDirection);
	void setConeDirectionZ(GLfloat newDirection);

	GLfloat getConeFallOff();
	void setConeFallOff(GLfloat newConeFallOff);

	glm::vec3 getAttenuation();
	void setAttenuationLinear(GLfloat newAttenuation);
	void setAttenuationQuadratic(GLfloat newAttenuation);
	
	int getStatus();
	void turnOff();
	void turnOn();
	
	int getType();
	void setType(int newType);
	
private:
	glm::vec4 m_ambient;
	glm::vec4 m_diffuse;
	glm::vec4 m_specular;
	glm::vec4 m_position;
	glm::vec3 m_coneDirection;
	GLfloat m_coneFallOff;
	glm::vec3 m_attenuation;	// y = linear, z = quadratic
	int m_active;
	int m_type; // 0 for point, 1 for spot, 2 for directional
};

