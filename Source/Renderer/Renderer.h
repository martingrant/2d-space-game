#pragma once

#define DEG_TO_RADIAN 0.017453293

#include <stack>
#include <sstream> 
#include <vector>
#include <memory>
#include <unordered_map>

#ifdef _WIN32
#include <SDL_image.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include "../Dirent/dirent.h"
#elif __APPLE__
#include <SDL2_image/SDL_image.h>
#include <OpenGL/gl3.h>
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#include <dirent.h>
#endif

#include "../Utilities/UtilitiesManager.h"
#include "../Entities/Entity.h"
#include "../Entities/Components/Component.h"
#include "../Entities/Components/PhysicalComponent.h"
#include "../Entities/Components/TextureComponent.h"
#include "../Entities/Components/PhysicsComponent.h"


class Renderer
{
	/* Class Constructors & Destructors */
public:
	Renderer(unsigned int windowWidth, unsigned int windowHeight);
	~Renderer();

public:
    void setUniform3f(GLuint shader, std::string uniform, const GLfloat* data);
	void setUniform4f(GLuint shader, std::string uniform, const GLfloat* data);
	void setUniformMatrix3fv(const GLuint program, const char* uniformName, const GLfloat *data);
	void setUniformMatrix4fv(const GLuint program, const char* uniformName, const GLfloat *data);
    void setDefaultUniforms();
    
	void drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive);

    void renderBegin();
    void renderEnd();
    void setUpQuad();
	void renderEntity(Entity* entity);
    void renderTexture(std::string name, float positionX, float positionY, unsigned int width, unsigned int height);
    void renderTexture2(std::string name, float positionX, float positionY, unsigned int width, unsigned int height);
    
    
    void sendProjectionMatrix();
    void setProjectionOrthographic(float left, float right, float bottom, float top);
	void setProjectionMatrix(float fieldOfView, float aspectX, float aspectY, float nearPlane, float farPlane); // todo should go in camera?
    
    void setViewMatrix(glm::mat4 viewMatrix);

private:
    GLuint m_quad;

	glm::mat4 m_projectionMatrix;
    
    std::stack<glm::mat4> m_stack;
    glm::mat4 m_modelview;
    
	/* Shaders */
public:
	void useShaderProgram(std::string shaderName);
	void loadShaders(const char* directoryPath);

private:
	char* checkForShaderErrors(const GLuint shader);
	char* checkForShaderProgramErrors(const GLuint shader);
	void loadShader(const char* shaderName, const char* filePathFrag, const char* filePathVert);
	char* loadFile(const char* filePath, GLint& fileSize);

private:
	std::unordered_map<std::string, GLuint> m_shaderProgramMap;
	std::unordered_map<std::string, GLuint> m_shaderMap;

	/* Textures */
public:
	void loadTexture(std::string name, std::string filePath);
	GLuint getTextureOGL(std::string name);
	void useTexture2D(std::string name);

private:
	std::unordered_map<std::string, GLuint> m_textureMap;
};

