#include "Renderer.h"


Renderer::Renderer(unsigned int windowWidth, unsigned int windowHeight)
{
	UtilitiesManager::getInstance().getMessageLog()->logString("System constructing: Renderer");
    
	m_shaderProgramMap.emplace("currentShader", -1);
	loadShaders("Source/Shaders");

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE); // make sure depth test is on
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
    
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    setUpQuad(); // default basic quad with texture coords
    setProjectionOrthographic(0.0f, 640.0f, 480.0f, 0.0f); // set projection matrix
    setDefaultUniforms(); // send projection matrix to all shaders
    
    UtilitiesManager::getInstance().getMessageLog()->logString("System constructed: Renderer");
}


Renderer::~Renderer()
{
    UtilitiesManager::getInstance().getMessageLog()->logString("System destructing: Renderer");
    
	// Delete shaders and clear map
	for (auto iterator = m_shaderMap.begin(); iterator != m_shaderMap.end(); ++iterator)
	{
		glDeleteShader(iterator->second);
	}
	m_shaderMap.clear();

	// Delete shader programs and clear map
	for (auto iterator = m_shaderProgramMap.begin(); iterator != m_shaderProgramMap.end(); ++iterator)
	{
		glDeleteProgram(iterator->second);
	}
	m_shaderProgramMap.clear();

	// Delete texture map and clear it
	for (auto iterator = m_textureMap.begin(); iterator != m_textureMap.end(); ++iterator)
	{
		glDeleteTextures(1, &iterator->second);
	}
	m_textureMap.clear();
    
    UtilitiesManager::getInstance().getMessageLog()->logString("System destructed: Renderer");
}


void Renderer::setUniform3f(GLuint shader, std::string uniform, const GLfloat* data)
{
    int uniformIndex = glGetUniformLocation(shader, uniform.c_str());
    glUniform3f(uniformIndex, data[0], data[1], data[2]);
}


void Renderer::setUniform4f(GLuint shader, std::string uniform, const GLfloat* data)
{
	int uniformIndex = glGetUniformLocation(shader, uniform.c_str());
	glUniform4f(uniformIndex, data[0], data[1], data[2], data[3]);
}


void Renderer::setUniformMatrix3fv(const GLuint program, const char* uniformName, const GLfloat *data)
{
	int uniformIndex = glGetUniformLocation(program, uniformName);
	glUniformMatrix3fv(uniformIndex, 1, GL_FALSE, data);
}


void Renderer::setUniformMatrix4fv(const GLuint program, const char* uniformName, const GLfloat *data) {
	int uniformIndex = glGetUniformLocation(program, uniformName);
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, data);
}


void Renderer::setDefaultUniforms()
{
    for (auto iterator = m_shaderProgramMap.begin(); iterator != m_shaderProgramMap.end(); ++iterator)
    {
        useShaderProgram(iterator->first);
        sendProjectionMatrix();
    }
}


void Renderer::drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive) {
	glBindVertexArray(mesh);	// Bind mesh VAO
	glDrawElements(primitive, indexCount, GL_UNSIGNED_INT, nullptr);	// draw VAO 
	glBindVertexArray(0);
}


void Renderer::renderBegin()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //m_stack.push(m_modelview);

    //m_stack.top() = glm::lookAt(m_camera->getPosition(), m_camera->getPosition() + m_camera->getFocus(), m_camera->getOrientation());

/*
	// Multiply light positions by modelview
	glm::vec4 tmp;
	glm::vec3 tmp2;
	glm::mat3 rotOnlyViewMatrix;
	for (int i = 0; i < 2; i++)
	{
		tmp = m_stack.top()*lightPositions[i];
		lightList[i].setPositionX(tmp.x);
		lightList[i].setPositionY(tmp.y);
		lightList[i].setPositionZ(tmp.z);

		rotOnlyViewMatrix = glm::mat3(m_stack.top());
		tmp2 = rotOnlyViewMatrix * glm::vec3(lightConeDirection[i].x, lightConeDirection[i].y, lightConeDirection[i].z); // passing cone direction or look at into eye coordinates
		lightList[i].setConeDirectionX(tmp2.x);
		lightList[i].setConeDirectionY(tmp2.y);
		lightList[i].setConeDirectionZ(tmp2.z);
	}
	useShaderProgram("multiplelight");
	setUniform(m_shaderProgramMap["currentShader"], lightList);
	setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "projection", glm::value_ptr(m_projectionMatrix));*/
	//useShaderProgram("textured");
}


void Renderer::renderEnd()
{
    //m_stack.pop();
}


void Renderer::setUpQuad()
{
    // Create Vertex Array Object
    //GLuint vao;
    glGenVertexArrays(1, &m_quad);
    glBindVertexArray(m_quad);
    
    // Create a Vertex Buffer Object and copy the vertex data to it
    GLuint vbo;
    glGenBuffers(1, &vbo);
    
    
    
    float vertices[] = {
        //  Position      Color             Texcoords
        -1.0f,  1.0f,  1.0f, 0.0f, 0.0f, 0.0f, 0.0f, // Top-left
        1.0f,   1.0f,  0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // Top-right
        1.0f,  -1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // Bottom-right
        -1.0f, -1.0f,  1.0f, 1.0f, 1.0f, 0.0f, 1.0f  // Bottom-left
    };
    
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    
    // Create an element array
    GLuint ebo;
    glGenBuffers(1, &ebo);
    
    GLuint elements[] = {
        0, 1, 2,
        2, 3, 0
    };
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);
    
    
    
    // Specify the layout of the vertex data
    GLint posAttrib = glGetAttribLocation(3, "in_position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), 0);
    
    GLint colAttrib = glGetAttribLocation(3, "in_color");
    glEnableVertexAttribArray(colAttrib);
    glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));
    
    GLint texAttrib = glGetAttribLocation(3, "in_texCoord");
    glEnableVertexAttribArray(texAttrib);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)(5 * sizeof(GLfloat)));
}


void Renderer::renderEntity(Entity* entity)
{
	glActiveTexture(GL_TEXTURE0);
    
    useShaderProgram("2d");

	TextureComponent* textureComponent = (TextureComponent*)entity->getComponent(TEXTURE);
	useTexture2D(textureComponent->getTextureName());

    PhysicsComponent* physicsComponent = (PhysicsComponent*)entity->getComponent(PHYSICS);
    
    glm::mat4 model = glm::mat4(1.0f);
    float x = physicsComponent->getPositionX();
    float y = physicsComponent->getPositionY();
    
    int w = physicsComponent->getWidth();
    int h = physicsComponent->getHeight();
    
    float r = physicsComponent->getAngle();
    
    
    
    model = glm::translate(model, glm::vec3(x * 10, y * 10, 1.0f));
    model = glm::scale(model, glm::vec3(w * 10, h * 10, 1.0f));
    
    model = glm::rotate(model, r, glm::vec3(0.0f, 0.0f, 1.0f));
    
    
	setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "model", glm::value_ptr(model));

	drawIndexedMesh(m_quad, 6, GL_TRIANGLES);
    
}


void Renderer::renderTexture(std::string name, float positionX, float positionY, unsigned int width, unsigned int height)
{
    //glDepthMask(GL_FALSE);
    
    //useShaderProgram("2d");
    
    useTexture2D(name);
    
    glm::mat4 model = glm::mat4(1.0f);
    
    model = glm::translate(model, glm::vec3(positionX * 10, positionY * 10, 1.0f));
    model = glm::scale(model, glm::vec3(width * 10, height * 10, 1.0f));
    
    setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "model", glm::value_ptr(model));
    
    drawIndexedMesh(m_quad, 6, GL_TRIANGLES);
}


void Renderer::renderTexture2(std::string name, float positionX, float positionY, unsigned int width, unsigned int height)
{
    glDepthMask(GL_FALSE);
    glActiveTexture(GL_TEXTURE0);
    useShaderProgram("2d");
    
    useTexture2D(name);
    
    glm::mat4 model = glm::mat4(1.0f);
    
    model = glm::translate(model, glm::vec3(positionX, positionY, 1.0f));
    model = glm::scale(model, glm::vec3(width, height, 1.0f));
    
    setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "model", glm::value_ptr(model));
    
    drawIndexedMesh(m_quad, 6, GL_TRIANGLES);
}


void Renderer::sendProjectionMatrix()
{
    setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "projection", glm::value_ptr(m_projectionMatrix));
}


void Renderer::setProjectionOrthographic(float left, float right, float bottom, float top)
{
    m_projectionMatrix = glm::ortho(left, right, bottom, top);
}


void Renderer::setProjectionMatrix(float fieldOfView, float aspectX, float aspectY, float nearPlane, float farPlane)
{
	m_projectionMatrix = glm::perspective(fieldOfView, aspectX / aspectY, nearPlane, farPlane);
}


void Renderer::setViewMatrix(glm::mat4 viewMatrix)
{
    setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "view", glm::value_ptr(viewMatrix));
}


void Renderer::useShaderProgram(std::string shaderName)
{
	bool shaderSet = false;

	for (auto iterator = m_shaderProgramMap.begin(); iterator != m_shaderProgramMap.end(); ++iterator)
	{
		if (iterator->first == shaderName)
		{
			glUseProgram(iterator->second);
			shaderSet = true;
			m_shaderProgramMap.at("currentShader") = m_shaderProgramMap[shaderName];
			//m_s->getMessageLog()->logString("Using shader '" + shaderName + "'.");
		}
	}

	if (shaderSet == false)
	{
		//UtilitiesManager::getInstance().getMessageLog()->logString("Could not use shader '" + shaderName + "'. Shader not found.");
	}
}


void Renderer::loadShaders(const char* directoryPath)
{
	std::vector<std::string> shaders;

    UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Attempting to load shaders located in:") + directoryPath);
    
	DIR *directory;
	struct dirent *entry;
	if ((directory = opendir(directoryPath)) != NULL)
	{
		/* print all the files and directories within directory */
		while ((entry = readdir(directory)) != NULL)
		{
			std::string s = entry->d_name;

			if (s.size() > 5) // Some records come up as "." and "..", this will disqualify them. Shader filenames will be at least 6 characters in length e.g. 1.vert
			{
				if (s != ".DS_Store") // Get rid of this file on OS X
					shaders.push_back(s);
			}
		}
		closedir(directory);

		for (unsigned int index = 0; index < shaders.size() - 1; index += 2) // 2 linked shaders are typically named the same so will be listed together.. might cause problems in future though
		{
			size_t findDot = shaders[index].find(".");
			std::string shaderName = shaders[index].substr(0, findDot);

			std::string vertexShader = directoryPath;
			vertexShader += "/";
			vertexShader += shaders[index];

			std::string fragmentShader = directoryPath;
			fragmentShader += "/";
			fragmentShader += shaders[index + 1];

			loadShader(shaderName.c_str(), vertexShader.c_str(), fragmentShader.c_str());
		}
        UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Loaded shaders located in: ") + directoryPath);
	}
	else
	{
        UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Couldn't open directory: ") + directoryPath);
	}
}


char* Renderer::checkForShaderErrors(const GLuint shader)
{
	int maximumLength = 0;
	int messageLength = 0;

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maximumLength);

	char* message = nullptr;

	if (maximumLength > 0)
	{
		message = new GLchar[maximumLength];
		glGetShaderInfoLog(shader, maximumLength, &messageLength, message);
	}

	return message;
}


char* Renderer::checkForShaderProgramErrors(const GLuint shader)
{
	int maximumLength = 0;
	int messageLength = 0;

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maximumLength);

	char* message = nullptr;

	if (maximumLength > 0)
	{
		message = new GLchar[maximumLength];
		glGetShaderInfoLog(shader, maximumLength, &messageLength, message);
	}

	return message;
}


void Renderer::loadShader(const char* shaderName, const char* filePathFrag, const char* filePathVert)
{
	GLint fileSize = 0;

	// Load Vertex
	const char* vertexSource = loadFile(filePathVert, fileSize);

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexSource, &fileSize);

	glCompileShader(vertexShader);

	// Load Fragment
	const char* fragmentSource = loadFile(filePathFrag, fileSize);

	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentSource, &fileSize);

	glCompileShader(fragmentShader);

	GLuint shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	glBindAttribLocation(shaderProgram, 0, "in_position");
	glBindAttribLocation(shaderProgram, 1, "in_colour");
	glBindAttribLocation(shaderProgram, 2, "in_normal");
	glBindAttribLocation(shaderProgram, 3, "in_texCoord");

	glBindFragDataLocation(shaderProgram, 0, "out_Colour");

	glLinkProgram(shaderProgram);

	// Check shaders and program were compiled correctly, clean up if not
	char* vertexShaderStatus = checkForShaderErrors(vertexShader);
	if (vertexShaderStatus == nullptr)
	{
        std::string message = "Compiled vertex shader: ";
        message += filePathVert;
        message += " as shader #";
        message += std::to_string((unsigned int)vertexShader);
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
	}
	else
	{
        std::string message = "Vertex shader #";
        message += std::to_string((unsigned int)vertexShader);
        message += " - ";
        message += filePathVert;
        message += " not compiled. Message: ";
        message += vertexShaderStatus;
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
        
        glDeleteShader(vertexShader);
	}

	char* fragmentShaderStatus = checkForShaderErrors(fragmentShader);
	if (fragmentShaderStatus == nullptr)
	{
        std::string message = "Compiled fragment shader: ";
        message += filePathFrag;
        message += " as shader #";
        message += std::to_string((unsigned int)fragmentShader);
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
	}
	else
	{
        std::string message = "Fragment shader #";
        message += std::to_string((unsigned int)fragmentShader);
        message += " - ";
        message += filePathFrag;
        message += " not compiled. Message: ";
        message += fragmentShaderStatus;
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
        
		glDeleteShader(fragmentShader);
	}

	const char* shaderProgramStatus = checkForShaderProgramErrors(shaderProgram);
	if (shaderProgramStatus == nullptr)
	{
        std::string message = "Created shader program #";
        message += std::to_string((unsigned int)shaderProgram);
        message += " '";
        message += shaderName;
        message += "' using shaders #";
        message += std::to_string((unsigned int)vertexShader);
        message += " and #";
        message += std::to_string((unsigned int)fragmentShader);
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);

		m_shaderMap[filePathVert] = vertexShader;
		m_shaderMap[filePathFrag] = fragmentShader;
		m_shaderProgramMap[shaderName] = shaderProgram;
	}
	else
	{
        std::string message = "Could not create shader program: ";
        message += shaderName;
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
        
		glDeleteProgram(shaderProgram);
	}

	// Delete the buffers for shader messages if they were filled
	if (vertexShaderStatus != nullptr)
	{
		delete[] vertexShaderStatus;
	}

	if (fragmentShaderStatus != nullptr)
	{
		delete[] fragmentShaderStatus;
	}

	if (vertexShaderStatus != nullptr)
	{
		delete[] shaderProgramStatus;
	}

	// come back and check all cases for these deletes, there is possible leaks

	delete[] vertexSource;
	delete[] fragmentSource;
}


char* Renderer::loadFile(const char* filePath, GLint& fileSize)
{
	std::ifstream fileStream(filePath, std::ifstream::binary);

	if (fileStream)
	{
		// get length of file:
		fileStream.seekg(0, fileStream.end);
		long long size = fileStream.tellg();
		fileSize = (GLint)size;
		fileStream.seekg(0, fileStream.beg);

		char* buffer = new char[fileSize];

		// read data as a block:
		fileStream.read(buffer, fileSize);

		fileStream.close();

        UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Loaded file: ") + filePath);

		return buffer;
	}
	else
	{
        UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Failed to load file: ") + filePath);
	}

	return nullptr;
}


void Renderer::loadTexture(std::string name, std::string filePath)
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	SDL_Surface *tmpSurface = IMG_Load(filePath.c_str());
    
	if (!tmpSurface)
    {
        UtilitiesManager::getInstance().getMessageLog()->logString("Error loading texture '" + name + "' from '" + filePath + "'.");
	}
    else
    {
        UtilitiesManager::getInstance().getMessageLog()->logString("Loaded texture '" + name + "' from path '" + filePath + "'.");
    }

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	SDL_PixelFormat *format = tmpSurface->format;

	GLuint externalFormat, internalFormat;
	if (format->Amask)
	{
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGBA : GL_BGRA;
	}
	else
	{
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGB : GL_BGR;
	}

	// todo always been using internalFormat then externalFormat in below line, causes lines over textures on mac, using GL_RGBA has fixed the bug
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tmpSurface->w, tmpSurface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);

	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer

	m_textureMap[name] = texID;
}


GLuint Renderer::getTextureOGL(std::string name)
{
	return m_textureMap[name];
}


void Renderer::useTexture2D(std::string name)
{
	// todo check if texture exists?
	glBindTexture(GL_TEXTURE_2D, m_textureMap[name]);
}
