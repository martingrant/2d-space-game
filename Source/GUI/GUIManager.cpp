#include "GUIManager.h"


GUIManager::GUIManager()
{
}


GUIManager::~GUIManager()
{
	for (auto iterator = m_elementMap.begin(); iterator != m_elementMap.end(); ++iterator)
	{
		delete iterator->second;
	}
}


void GUIManager::update()
{
	static std::shared_ptr<Input> input = UtilitiesManager::getInstance().getInputManager();

	for (auto iterator = m_elementMap.begin(); iterator != m_elementMap.end(); ++iterator)
	{
		if (iterator->second->mouseOverlap(input->getMousePosition().first, input->getMousePosition().second) == true)
		{
			if (input->mouseButtonState(1))
			{
                iterator->second->activate();
			}
		}
        else
        {
            iterator->second->deactivate();
        }
	}
}


GUIElement* GUIManager::getGUIElement(std::string name)
{
	return m_elementMap[name];
}


void GUIManager::createElement(std::string name, GUIElementTypes elementType, std::string texture, float positionX, float positionY, float width, float height)
{
	switch (elementType)
	{
		case BUTTON:
			m_elementMap[name] = new GUIButton(texture, positionX, positionY, width, height);
			break;
		//default:
	}
}