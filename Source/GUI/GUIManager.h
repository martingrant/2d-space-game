#pragma once

#include <unordered_map>
#include <string>
#include <memory>
#include <string>

#include "../Utilities/UtilitiesManager.h"
#include "GUIElement.h"
#include "GUIElementTypes.h"
#include "GUIButton.h"

class GUIManager
{
public:
	GUIManager();
	~GUIManager();

public:
	void update();
	GUIElement* getGUIElement(std::string name);
	void createElement(std::string name, GUIElementTypes elementType, std::string texture, float positionX, float positionY, float width, float height);

private:
	std::unordered_map<std::string, GUIElement*> m_elementMap;
};

