#include "GUIButton.h"


GUIButton::GUIButton(std::string texture, float positionX, float positionY, float width, float height)
{
	m_positionX = positionX;
	m_positionY = positionY;
	m_width = width;
	m_height = height;

	m_texture = texture;
    m_active = false;
}


GUIButton::~GUIButton()
{
}
