#include "Application/Application.h"


int main(int argc, char *args[])
{
	Application* game = new Application();

	while (game->run())
		continue;
    
    delete game;
    
	return 0;
}