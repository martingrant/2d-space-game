#include "TestingScreen.h"


TestingScreen::TestingScreen(std::shared_ptr<Renderer> renderer) : m_renderer(renderer)
{
    m_renderer->loadTexture("white", "Resources/Textures/white.png");
	m_renderer->loadTexture("metal", "Resources/Textures/metal.bmp");
    m_renderer->loadTexture("crosshair", "Resources/Textures/crosshair3.png");
    m_renderer->loadTexture("texttexture", "Resources/Textures/texttexture.png");
	m_renderer->loadTexture("button", "Resources/Textures/button.png");
	m_renderer->loadTexture("red", "Resources/Textures/red.png");

	m_entityManager = new EntityManager();
    
    b2Vec2 grav(0.0f, 0.0f);
    world = new b2World(grav);
    
    
    // player entity
	m_entityManager->createEntity("player");
	m_entityManager->addComponentToEntityName("player", PHYSICAL);
	PhysicalComponent* physicalComponent = (PhysicalComponent*)m_entityManager->getEntity("player")->getComponent(PHYSICAL);
    physicalComponent->setPosition(glm::vec2(0.0f, 0.0f));
    physicalComponent->setScale(glm::vec2(100.0f, 100.0f));
    
	m_entityManager->addComponentToEntityName("player", TEXTURE);
	TextureComponent* textureComponent = (TextureComponent*)m_entityManager->getEntity("player")->getComponent(TEXTURE);
	textureComponent->setTextureName("red");
    
    m_entityManager->addComponentToEntityName("player", PLAYER);
    PlayerComponent* playerComponent = (PlayerComponent*)m_entityManager->getEntity("player")->getComponent(PLAYER);
    playerComponent->getUserInput(UtilitiesManager::getInstance().getInputManager());
    
    m_entityManager->addComponentToEntityName("player", PHYSICS);
    PhysicsComponent* physicsComponent = (PhysicsComponent*)m_entityManager->getEntity("player")->getComponent(PHYSICS);
    physicsComponent->setProperties(*world, "dynamic", 20.0f, 40.0f, 1.0f, 1.0f, 1.0f, 0.6f, 0.0f, false);
    
    m_entityManager->addComponentToEntityName("player", HEALTH);
    HealthComponent* healthComponent = (HealthComponent*)m_entityManager->getEntity("player")->getComponent(HEALTH);
    healthComponent->setHealth(100);
    
    
    
    // enemy entity
    m_entityManager->createEntity("enemy");
    m_entityManager->addComponentToEntityName("enemy", PHYSICAL);
    PhysicalComponent* physicalComponent2 = (PhysicalComponent*)m_entityManager->getEntity("enemy")->getComponent(PHYSICAL);
    physicalComponent2->setPosition(glm::vec2(0.0f, 0.0f));
    physicalComponent2->setScale(glm::vec2(100.0f, 100.0f));
    
    m_entityManager->addComponentToEntityName("enemy", TEXTURE);
    TextureComponent* textureComponent2 = (TextureComponent*)m_entityManager->getEntity("enemy")->getComponent(TEXTURE);
    textureComponent2->setTextureName("red");
    
    m_entityManager->addComponentToEntityName("enemy", PHYSICS);
    PhysicsComponent* physicsComponent2 = (PhysicsComponent*)m_entityManager->getEntity("enemy")->getComponent(PHYSICS);
    physicsComponent2->setProperties(*world, "dynamic", 20.0f, 20.0f, 1.0f, 1.0f, 0.9f, 0.6f, 0.0f, false);
    
    m_entityManager->addComponentToEntityName("enemy", HEALTH);
    HealthComponent* healthComponent3 = (HealthComponent*)m_entityManager->getEntity("enemy")->getComponent(HEALTH);
    healthComponent3->setHealth(100);
    
    
	guiManager = new GUIManager();

	guiManager->createElement("menubutton", BUTTON, "button", 0, 0, 80, 80);
    
    m_camera = new Camera2D();
    
    m_loader = new TMXLoader();
    
    m_loader->loadMap("level1", "Resources/Levels/level1.tmx");
    
    char tileID = 0;
    
    int tileWidth = m_loader->getMap("level1")->getTileWidth();
    int tileHeight = m_loader->getMap("level1")->getTileHeight();
    
    int tile = 0;
    
    for (int i = 0; i < m_loader->getMap("level1")->getWidth(); ++i)
    {
        for (int j = 0; j < m_loader->getMap("level1")->getHeight(); ++j)
        {
            // get the tile at current position
            tileID = m_loader->getMap("level1")->getTileLayer("Tile Layer 1")->getTileVector()[i][j];
            
            // only render if it is an actual tile (tileID = 0 means no tile / don't render anything here)
            if (tileID > 0)
            {
                
                std::string tilestr = "tile"; tilestr += std::to_string(tile);
                
                m_entityManager->createEntity(tilestr.c_str());

                m_entityManager->addComponentToEntityName(tilestr.c_str(), TEXTURE);
                TextureComponent* textureComponent4 = (TextureComponent*)m_entityManager->getEntity(tilestr.c_str())->getComponent(TEXTURE);
                textureComponent4->setTextureName("metal");
                
                m_entityManager->addComponentToEntityName(tilestr.c_str(), PHYSICS);
                PhysicsComponent* physicsComponent4 = (PhysicsComponent*)m_entityManager->getEntity(tilestr.c_str())->getComponent(PHYSICS);
                physicsComponent4->setProperties(*world, "static", j * 2, i * 2, 1, 1, 1.0f, 0.0f, 0.0f, false);
                
                tile++;
            }
        }
    }
    
    listen = new MyContactListener();
    
    world->SetContactListener(listen);
}


TestingScreen::~TestingScreen()
{
    
}


void TestingScreen::update()
{
    world->Step((1.0f / 30.0f), 10, 8);
    world->ClearForces();
    
    m_entityManager->update();
    
	guiManager->update();
    
    if (guiManager->getGUIElement("menubutton")->isActive()) UtilitiesManager::getInstance().getWindowManager()->setCurrentScreen("MenuScreen");
    
    m_camera->update(UtilitiesManager::getInstance().getInputManager());
    m_renderer->setViewMatrix(m_camera->getViewMatrix());
    
    handleBullets();
    static bool fire = true;
    if (UtilitiesManager::getInstance().getInputManager()->getKeyState("space") && fire)
    {
        fire = false;
        
        PhysicsComponent* physics = (PhysicsComponent*)m_entityManager->getEntity("player")->getComponent(PHYSICS);
        float x = physics->getPositionX();
        float y = (physics->getPositionY() - 10);

        static int bulletnum = 0;
        std::string name = "bullet";
        name += std::to_string(bulletnum);
        bulletnum++;
        m_entityManager->createEntity(name);
        m_entityManager->addComponentToEntityName(name, TEXTURE);
        
        TextureComponent* textureComponent2 = (TextureComponent*)m_entityManager->getEntity(name)->getComponent(TEXTURE);
        textureComponent2->setTextureName("red");
        
        m_entityManager->addComponentToEntityName(name, PHYSICS);
        b2Vec2 pt = b2Vec2(x, y);
        PhysicsComponent* physicsComponent2 = (PhysicsComponent*)m_entityManager->getEntity(name)->getComponent(PHYSICS);
        physicsComponent2->setProperties(*world, "dynamic", x, y, 1.0f, 1.0f, 0.9f, 0.1f, 0.0f, true);
        physicsComponent2->applyLinearImpluse(b2Vec2(0.0f, -100.0f), pt);
    }
    
    
    static int t = 0;
    t++;
    if (t > 50)
    {
        t = 0;
        fire = true;
    }
    
    
    if (listen->reducehp == true)
    {
        if (m_entityManager->getEntity("enemy") != nullptr)
        {
            HealthComponent* healthComponent = (HealthComponent*)m_entityManager->getEntity("enemy")->getComponent(HEALTH);
            
            if (healthComponent->getHealth() != 0)
                healthComponent->incrementHealth(-50);
        }
    }
    
    
    static int t2 = 0;
    t2++;
    if (t2 > 200)
    {
        t2 = 0;
        
        HealthComponent* healthComponent = (HealthComponent*)m_entityManager->getEntity("player")->getComponent(HEALTH);
        
        std::cout << "player hp: " << healthComponent->getHealth() << std::endl;
        
        if (m_entityManager->getEntity("enemy") != nullptr)
        {
            healthComponent = (HealthComponent*)m_entityManager->getEntity("enemy")->getComponent(HEALTH);
            
            std::cout << "enemy hp: " << healthComponent->getHealth() << std::endl;
        }
    }
    
    for (int i = 0; i < m_entityManager->getEntityVector().size(); ++i)
    {
        if (m_entityManager->getEntityVector()[i].getDestroy() == true)
        {
            PhysicsComponent* pc = (PhysicsComponent*)m_entityManager->getEntityVector()[i].getComponent(PHYSICS);
            
            world->DestroyBody(pc->getBody());
            
            m_entityManager->getEntityVector()[i].unassign();
        }
    }
}


void TestingScreen::handleBullets()
{
    
}


void TestingScreen::render()
{
    m_renderer->renderTexture2("texttexture", 300.0f, 100.0f, 200.0f, 100.0f);
    m_renderer->renderTexture2(guiManager->getGUIElement("menubutton")->getTexture(), guiManager->getGUIElement("menubutton")->getPositionX(), guiManager->getGUIElement("menubutton")->getPositionY(), guiManager->getGUIElement("menubutton")->getWidth(), guiManager->getGUIElement("menubutton")->getHeight());
    // gui works but needs to be rendered not in-world
 
    
    for (int i = 0; i < m_entityManager->getNumberOfEntities(); ++i)
    {
        if (m_entityManager->getEntity(i)->isAssigned())
            m_renderer->renderEntity(m_entityManager->getEntity(i));
    }
}