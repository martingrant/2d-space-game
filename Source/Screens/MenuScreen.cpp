#include "MenuScreen.h"


MenuScreen::MenuScreen(std::shared_ptr<Renderer> renderer) : m_renderer(renderer)
{
	m_renderer->loadTexture("startbutton", "Resources/Textures/startbutton.png");
    
	guiManager = new GUIManager();

	guiManager->createElement("startbutton", BUTTON, "startbutton", 300, 100, 200, 80);
}


MenuScreen::~MenuScreen()
{
    
}


void MenuScreen::update()
{
	guiManager->update();

    if (guiManager->getGUIElement("startbutton")->isActive()) UtilitiesManager::getInstance().getWindowManager()->setCurrentScreen("TestingScreen");
}


void MenuScreen::render()
{
	m_renderer->renderTexture2(guiManager->getGUIElement("startbutton")->getTexture(), guiManager->getGUIElement("startbutton")->getPositionX(), guiManager->getGUIElement("startbutton")->getPositionY(), guiManager->getGUIElement("startbutton")->getWidth(), guiManager->getGUIElement("startbutton")->getHeight());
}