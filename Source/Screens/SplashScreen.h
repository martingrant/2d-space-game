//
//  SlashScreen.h
//  SpaceGame2D
//
//  Created by Marty on 19/11/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#pragma once

#include <stack>
#include <sstream>
#include <cmath>
#include <sstream>

#ifdef _WIN32
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#elif __APPLE__
#include <SDL2/SDL.h>
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#endif

#include "Screen.h"
#include "../Renderer/Renderer.h"
#include "../Utilities/UtilitiesManager.h"
#include "../Camera/Camera2D.h"

class SplashScreen : public Screen
{
    /* Constructors & Destructors */
public:
    SplashScreen(std::shared_ptr<Renderer> renderer) {
        m_renderer = renderer;
        //m_renderer->loadTexture("texttexture", "Resources/Textures/texttexture.png");
        camera = new Camera2D();
    }
    ~SplashScreen() { }
    
    /* General Public Methods */
public:
    virtual void update() {
        m_renderer->setViewMatrix(camera->getViewMatrix());
    }
    virtual void render()
    {
        m_renderer->renderTexture2("startbutton", 300.0f, 300.0f, 200.0f, 80.0f);
    }
    
private:
    Camera2D* camera;
    std::shared_ptr<Renderer> m_renderer;
};