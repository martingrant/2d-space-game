#include "SDLWindowManager.h"


SDLWindowManager::SDLWindowManager()
{
    UtilitiesManager::getInstance().getMessageLog()->logString("System constructing: SDLScreenManager.");
    
    m_windowTitle = UtilitiesManager::getInstance().getConfigLoader()->getValue("windowTitle");
    m_windowWidth = atoi(UtilitiesManager::getInstance().getConfigLoader()->getValue("windowWidth").c_str());
    m_windowHeight = atoi(UtilitiesManager::getInstance().getConfigLoader()->getValue("windowHeight").c_str());
    m_displayDeviceIndex = atoi(UtilitiesManager::getInstance().getConfigLoader()->getValue("displayDevice").c_str());
    m_openGLVersionMajor = atoi(UtilitiesManager::getInstance().getConfigLoader()->getValue("openGLMajor").c_str());
    m_openGLVersionMinor = atoi(UtilitiesManager::getInstance().getConfigLoader()->getValue("openGLMinor").c_str());
    
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		std::cout << "SDL_Init: " << SDL_GetError() << std::endl;
	}

    createWindow();

	m_renderer = std::shared_ptr<Renderer>(new Renderer(m_windowWidth, m_windowHeight));
    //m_renderer->setProjectionMatrix(60.0f, 800.0f, 600.0f, 1.0f, 150.0f);

	m_screenMap["TestingScreen"] = std::shared_ptr<TestingScreen>(new TestingScreen(m_renderer));
	m_screenMap["MenuScreen"] = std::shared_ptr<MenuScreen>(new MenuScreen(m_renderer));
    m_screenMap["SplashScreen"] = std::shared_ptr<SplashScreen>(new SplashScreen(m_renderer));
	m_screenMap["CurrentScreen"] = m_screenMap["TestingScreen"];

    m_running = true;
   
	UtilitiesManager::getInstance().getMessageLog()->logString("System constructed: SDLScreenManager.");
}


SDLWindowManager::~SDLWindowManager()
{
	UtilitiesManager::getInstance().getMessageLog()->logString("System destructing: SDLScreenManager.");

	SDL_GL_DeleteContext(m_GLContext);
	SDL_DestroyWindow(m_window);
	SDL_Quit();

	UtilitiesManager::getInstance().getMessageLog()->logString("System destructed: SDLScreenManager.");
}


bool SDLWindowManager::update()
{
    while (SDL_PollEvent(&m_SDLEvent))
    {
        switch (m_SDLEvent.type)
        {
            case SDL_WINDOWEVENT:
                switch (m_SDLEvent.window.event)
            {
                case SDL_WINDOWEVENT_CLOSE:
                    m_running = false;
                    break;
            }
                break;
            case SDL_QUIT:
                m_running = false;
                break;
        }
    }
    m_screenMap["CurrentScreen"]->update();

	static bool firstmouse = true;

	if (firstmouse)
	{
		SDL_WarpMouseInWindow(m_window, m_windowWidth / 2, m_windowHeight / 2);
		firstmouse = false;
	}
    
    return m_running;
}

/*
 * Renders the current screen.
 */
void SDLWindowManager::render()
{
    m_renderer->renderBegin();
    
	m_screenMap["CurrentScreen"]->render();
    
    SDL_GL_SwapWindow(m_window);

	m_renderer->renderEnd();
}


void SDLWindowManager::setCurrentScreen(std::string screen)
{
	m_screenMap["CurrentScreen"] = m_screenMap[screen];
}



void SDLWindowManager::createWindow()
{
	setUpDisplayModeVector();

	Uint32 windowFlags = 0;

#if defined(_DEBUG) || defined(DEBUG)
	windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
#else
	windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN;
	m_windowWidth = m_displayModeVector[0].w;
	m_windowHeight = m_displayModeVector[0].h;
#endif // #if defined(_DEBUG) || defined(DEBUG)

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, m_openGLVersionMajor);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, m_openGLVersionMinor);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    
	// Get number of displays, if more than 1 then create window on display device 1 (second monitor)
	if (SDL_GetNumVideoDisplays() > 1)
	{
		m_displayDeviceIndex = 1;
	}
	else
	{
		m_displayDeviceIndex = 0;
	}

	m_window = SDL_CreateWindow(m_windowTitle.c_str(), SDL_WINDOWPOS_CENTERED_DISPLAY(m_displayDeviceIndex), SDL_WINDOWPOS_CENTERED_DISPLAY(m_displayDeviceIndex), m_windowWidth, m_windowHeight, windowFlags);
    
	if (m_window == NULL)
	{
		std::cout << "SDL_CreateWindow: " << SDL_GetError() << std::endl;
	}

	m_GLContext = SDL_GL_CreateContext(m_window); // Create OpenGL context and attach to window
    
    std::cout << glGetError();
    std::cout << "OpenGL: " << glGetString(GL_VERSION) << std::endl << std::endl;

	SDL_GL_SetSwapInterval(1);

	/* If Windows, use GLEW to access modern OpenGL */
	#ifdef _WIN32
		glewExperimental = GL_TRUE;
		GLenum err = glewInit();
		if (GLEW_OK != err)
		{
			std::cout << "glewInit failed, aborting." << std::endl;
			exit(1);
		}
	#endif
}


void SDLWindowManager::setWindowTitle(std::string title)
{
	m_windowTitle = title;
	SDL_SetWindowTitle(m_window, m_windowTitle.c_str());
}


void SDLWindowManager::setWindowSize(int width, int height)
{
	m_windowWidth = width;
	m_windowHeight = height;
	SDL_SetWindowSize(m_window, m_windowWidth, m_windowHeight);
}


void SDLWindowManager::setWindowWidth(int width)
{
	m_windowWidth = width;
	SDL_SetWindowSize(m_window, m_windowWidth, m_windowHeight);
}


void SDLWindowManager::setWindowHeight(int height)
{
	m_windowHeight = height; 
	SDL_SetWindowSize(m_window, m_windowWidth, m_windowHeight);
}


void SDLWindowManager::minimiseWindow()
{
	SDL_MinimizeWindow(m_window);
}


void SDLWindowManager::restoreWindow()
{
	SDL_RestoreWindow(m_window);
}


void SDLWindowManager::maximiseWindow()
{
	SDL_MaximizeWindow(m_window);
}


void SDLWindowManager::setFullScreenMode()
{
	SDL_SetWindowFullscreen(m_window, SDL_WINDOW_FULLSCREEN);
}


void SDLWindowManager::setWindowedMode()
{
	SDL_SetWindowFullscreen(m_window, 0);
}


void SDLWindowManager::setUpDisplayModeVector()
{
	unsigned int numberOfDisplayModes = SDL_GetNumDisplayModes(m_displayDeviceIndex);
	SDL_DisplayMode tempDisplayMode;
	std::vector<SDL_DisplayMode> tempVector;
	unsigned int tempHeight = 0;
	unsigned int tempWidth = 0;

	// If function has been called before (vectors have been used) then clear them for next call
	// Used when game window has changed to another screen index
	if (m_displayModeVector.size() > 0 && tempVector.size() > 0)
	{
		m_displayModeVector.clear();
		tempVector.clear();
	}

	// Fill array with all display modes available to current screen
	for (unsigned int index = 0; index < numberOfDisplayModes; ++index)
	{
		SDL_GetDisplayMode(m_displayDeviceIndex, index, &tempDisplayMode);

		tempVector.push_back(tempDisplayMode);
	}

	// Check for duplicates by comparing height and width values, adding unique values to the member vector
	for (unsigned int index = 0; index < tempVector.size(); ++index)
	{
		if (index == 0)
		{
			m_displayModeVector.push_back(tempVector[index]);
			tempHeight = tempVector[index].h;
			tempWidth = tempVector[index].w;
		}
		else
		{
			if (tempHeight != tempVector[index].h /*&& tempWidth != tempVector[index].w*/)
			{
				m_displayModeVector.push_back(tempVector[index]);
				tempHeight = tempVector[index].h;
				tempWidth = tempVector[index].w;
			}
		}
	}
}