#pragma once

#define DEG_TO_RADIAN 0.017453293

#include <stack>
#include <sstream> 
#include <cmath>
#include <sstream>

#ifdef _WIN32
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#elif __APPLE__
#include <SDL2/SDL.h>
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#endif

#include "Screen.h"
#include "../Renderer/Renderer.h"
#include "../Input/SDLInput.h"
#include "../Utilities/UtilitiesManager.h"
#include "../Entities/Components/PhysicalComponent.h"
#include "../Entities/Entity.h"
#include "../Entities/EntityManager.h"
#include "../GUI/GUIManager.h"
#include "../GUI/GUIElementTypes.h"
#include "../Camera/Camera2D.h"
#include "../TMX/TMXLoader.h"


#include "../Entitiesv2/AbstractSystem.h"
#include "../Entitiesv2/TestSystem.h"
#include "../Entitiesv2/TestComponent.h"

#include <Box2D/Box2D.h>


struct Entitystatus
{
    bool shoulddestroy = false;
};



class MyContactListener : public b2ContactListener
{
public:
    MyContactListener() { }
    
    void BeginContact(b2Contact* contact) {
        if (contact->GetFixtureA()->GetDensity() == 0.9f && contact->GetFixtureB()->GetDensity() == 0.9f)
        {
            //reducehp = true; // todo - bug, hp will reduce if 2 bullets are colliding
    /*
        Entity* e1 = (Entity*)contact->GetFixtureA()->GetBody()->GetUserData();
        Entity* e2 = (Entity*)contact->GetFixtureB()->GetBody()->GetUserData();
        
        if (contact->IsTouching())
            int i = 1;
        
        if (e1 != nullptr)
            if (e1->isAssigned())
                if (e1->getEntityName() == "enemy")
                    e1->setDestroy(true);
        
        if (e2 != nullptr)
            if (e2->isAssigned())
                if (e2->getEntityName() == "enemy")
                    e2->setDestroy(true);
        }*/
        
        }
        PhysicsComponent* p1 = (PhysicsComponent*)contact->GetFixtureA()->GetBody()->GetUserData();
        PhysicsComponent* p2 = (PhysicsComponent*)contact->GetFixtureB()->GetBody()->GetUserData();
        
        if (contact->IsTouching())
        {
        if (p1->isBullet())
        {
            if (p2->getOwner()->getEntityName() == "enemy")
                p2->hasBeenHit();
            
            p1->getOwner()->setDestroy(true);
        }
        
        if (p2->isBullet())
        {
            if (p1->getOwner()->getEntityName() == "enemy")
                p1->hasBeenHit();
            
                        p2->getOwner()->setDestroy(true);
        }
        }
        
        //if (p1->getOwner()->getEntityName() == "enemy" || p2->getOwner()->getEntityName() == "enemy" )
           // int i  =1;
    }
    
    void EndContact(b2Contact* contact) {
        reducehp = false;  
    }
    
    bool reducehp = false;
    
};

class TestingScreen : public Screen, b2ContactListener
{
	/* Constructors & Destructors */
public:
	TestingScreen(std::shared_ptr<Renderer> renderer);
	~TestingScreen();

	/* General Public Methods */
public:
	virtual void update();
	virtual void render();
    
    void handleBullets();

private:
	std::shared_ptr<Renderer> m_renderer;

	EntityManager* m_entityManager;

	GUIManager* guiManager;
    
    Camera2D* m_camera;
    
    b2World* world;
    
    std::vector<b2Body*> bulletvector;
    
    TMXLoader* m_loader;
    
    MyContactListener* listen;
};




