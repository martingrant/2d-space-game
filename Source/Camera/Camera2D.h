//
//  Camera2D.hpp
//  SpaceGame2D
//
//  Created by Marty on 25/10/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#pragma once

#include <memory>

#ifdef _WIN32
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#elif __APPLE__
#include <OpenGL/gl3.h>
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#endif

#include "../Input/Input.h"

class Camera2D
{
public:
    Camera2D();
    ~Camera2D();
    
public:
    void update(std::shared_ptr<Input> input);
    
public:
    glm::mat4 getViewMatrix();
    
private:
    glm::mat4 m_viewMatrix;
    
    int m_positionX;
    int m_positionY;
};