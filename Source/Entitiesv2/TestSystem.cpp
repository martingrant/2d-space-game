//
//  TestSystem.cpp
//  SpaceGame2D
//
//  Created by Marty on 16/11/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#include "TestSystem.h"

TestSystem::TestSystem(unsigned int maximumComponents)
{
    m_maximumComponents = maximumComponents;
    m_componentVector.reserve(m_maximumComponents);
    
    for (unsigned int index = 0; index < m_maximumComponents; ++index)
    {
        m_componentVector.emplace_back(index);
    }
}


TestSystem::~TestSystem()
{
    
}


void TestSystem::update()
{
    for (unsigned int index = 0; index < m_componentVector.size(); ++index)
    {
        m_componentVector[index].update();
    }
}


unsigned int TestSystem::orderNewComponent()
{
    for (unsigned int index = 0; index < m_componentVector.size(); ++index)
    {
        if (m_componentVector[index].isAssigned() == false)
        {
            m_componentVector[index].assign();
            return m_componentVector[index].getID();
        }
    }
    
    return -1;
}