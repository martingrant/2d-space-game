//
//  AbstractComponent.h
//  SpaceGame2D
//
//  Created by Marty on 16/11/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#pragma once

class AbstractComponent
{
    /* Class Constructor & Destructor */
public:
    AbstractComponent()
    {
        m_ID = -1;
        m_assigned = false;
    }
    virtual ~AbstractComponent() { };
    
    /* General Public Methods*/
public:
    virtual void update() = 0;
    
public:
    unsigned int getID() { return m_ID; }
    void assign() { m_assigned = true; }
    void unAssign() { m_assigned = false; }
    bool isAssigned() { return m_assigned; }
    
protected:
    unsigned int m_ID;
    bool m_assigned;
};