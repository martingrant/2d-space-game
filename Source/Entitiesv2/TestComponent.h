//
//  TestComponent.h
//  SpaceGame2D
//
//  Created by Marty on 16/11/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#pragma once

#include "AbstractComponent.h"

class TestComponent : public AbstractComponent
{
public:
    TestComponent(unsigned int ID) { m_ID = ID; }
    virtual ~TestComponent() { }
    
public:
    void update() { }
};