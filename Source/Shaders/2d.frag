#version 330

// Some drivers require the following
precision highp float;



layout(location = 0) out vec4 out_Colour;

in vec2 ex_TexCoord;

uniform sampler2D tex;

void main(void) 
{    
	out_Colour = texture(tex, ex_TexCoord) * vec4(1.0, 1.0, 1.0, 1.0);
}