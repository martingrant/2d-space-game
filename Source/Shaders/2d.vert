#version 330

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

in vec3 in_position;

in vec2 in_texCoord;
out vec2 ex_TexCoord;

void main(void)
{
    ex_TexCoord = in_texCoord;
    
    //gl_Position = vec4(in_position.x, in_position.y, 0.0, 1.0);
    
    vec4 vertexPosition = projection * view * model * vec4(in_position, 1.0);
    gl_Position = vertexPosition;
}