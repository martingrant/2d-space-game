# README #

## Summary ##

* 3D OpenGL C++ Engine being developed by Martin Grant
* @_martingrant, www.midnightpacific.com
* Currently being developed on OS X 10.10.3 and Windows 7.

// come back and update the following list

## Libraries/Frameworks ##
* Should be installed in C:/dev
* [SDL 2.0.3](http://www.libsdl.org/download-2.0.php)
* [GLEW 1.10.0](http://glew.sourceforge.net/)
* [FMOD 4.44.35](https://bitbucket.org/E219/prototype-game/downloads/FMOD-4.44.35.zip) - [Website](http://www.fmod.org/download/#FMODExAPIDownloads)
* [Assimp 3.0](http://assimp.sourceforge.net/main_downloads.html)
* [SDL_image 2.0](https://www.libsdl.org/projects/SDL_image/)
* [RapidXML 1.13](http://rapidxml.sourceforge.net/)